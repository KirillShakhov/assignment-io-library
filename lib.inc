section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rdi, rax
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .loop

    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, 1
    mov rdx, 1
    mov rax, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r9
    mov r9, rsp
    push 0
    mov rax, rdi
    mov rdi, 10

    .loop:
    mov rdx, 0
    div rdi
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    cmp rax, 0
    je .out
    jmp .loop

    .out:
    mov rdi, rsp
    push r9
    call print_string
    pop r9
    mov rsp, r9
    pop r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push r9
    cmp rdi, 0
    jns .print
    mov r9, rdi
    mov rdi, '-'
    push r9
    call print_char
    pop r9
    mov rdi, r9
    neg rdi

    .print:
    call print_uint
    pop r9
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r8

    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi

    mov r8, rax
    xchg rdi, rsi

    push r8
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    pop r8

    cmp rax, r8
    jne .not_equal
    cmp rax, 0
    je .equal

    .check:
    mov r9b, byte[rdi]
    mov r10b, byte[rsi]
    cmp r9b, r10b
    jne .not_equal
    inc rdi
    inc rsi
    dec rax
    cmp rax, 0
    jne .check

    .equal:
    mov rax, 1
    jmp .end

    .not_equal:
    mov rax, 0


    .end:
    pop r8
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    push rax
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r10
    push r12
    push r13
    mov r10, rdi ; buf addr
    mov r13, rsi ; buf size
    mov r12, 0 ; counter
    jmp .loop

    .check:
    cmp r12, 0
    jne .check_length

    .loop:

    push r13
    push r10
    push r12
    call read_char
    pop r12
    pop r10
    pop r13

    cmp rax, 0x20
    je .check
    cmp rax, 0x9
    je .check
    cmp rax, 0xA
    je .check
    cmp rax, 0
    je .check_length
    mov [r10+r12], rax
    inc r12
    jmp .loop

    .check_length:
    cmp r13, r12
    jnge .error

    .success:
    mov [r10+r12], byte 0
    mov rax, r10
    mov rdx, r12
    jmp .end

    .error:
    mov rax, 0

    .end:
    pop r13
    pop r12
    pop r10
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    call string_length
    mov rdx, rax
    mov rcx, 0
    mov rax, 0

    .loop:
    mov r9b, byte[rdi+rcx]
    cmp r9b, '0'
    jnge .end
    cmp r9b, '9'
    jnle .end
    sub r9b, '0'
    imul rax, 10
    add al, r9b
    inc rcx
    cmp rcx, rdx
    jne .loop
    ret

    .end:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi
    call string_length
    pop rdi

    mov rdx, rax
    mov rcx, 0
    mov rax, 0
    cmp byte[rdi], '-'
    je .sign
    jmp parse_uint

    .sign:
    inc rdi

    call parse_uint

    cmp rdx, 0
    je .end
    inc rdx
    not rax
    inc rax

    .end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rcx
    push rsi
    call string_length
    pop rsi
    pop rcx
    pop rdi

    inc rax
    mov rcx, 0
    cmp rdx, rax
    jnge .error

    .loop:
    mov r9b, byte[rdi+rcx]
    mov byte[rsi+rcx], r9b
    inc rcx
    cmp rcx, rax
    jne .loop
    dec rax
    ret

    .error:
    mov rax, 0
    ret
